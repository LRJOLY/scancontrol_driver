cmake_minimum_required(VERSION 3.8)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

project(micro_epsilon_scancontrol_msgs)

find_package(ament_cmake REQUIRED)
find_package(rosidl_default_generators REQUIRED)


rosidl_generate_interfaces(${PROJECT_NAME}
  "srv/GetAvailableResolutions.srv"
  "srv/SetResolution.srv"
  "srv/GetResolution.srv"
  "srv/SetFeature.srv"
  "srv/GetFeature.srv"
)



ament_export_dependencies(rosidl_default_runtime)
ament_package()
