# micro_epsilon_scancontrol
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

## Overview

ROS2 device driver for the scanCONTROL series of laser line scanners of Micro Epsilon using the [scanCONTROL Linux C++ SDK 1.0]. The driver allows to connect to a (specific) scanCONTROL device, configure the sensor using predefined settings or at runtime and publishes the sensor data as point clouds. 

**Author: LR. Joly (Gitlab username: LRJOLY)**
All the work is based on a previous ROS driver developped by D. Kroezen (d.kroezen@tudelft.nl) [SAM|XL](https://samxl.com/), [TU Delft](https://tudelft.nl/)<br />


The micro_epsilon_scancontrol package has been tested under [ROS2] Humble and Ubuntu 22.04. with the profilometers 3000_100 and 3000_430.

## Installation

#### Dependencies

- [Aravis 0.8.x](https://github.com/AravisProject/aravis)
- [scanCONTROL Linux C++ SDK 1.0.0](https://www.micro-epsilon.com/2D_3D/laser-scanner/Software/downloads/) 


For Aravis, you have to type the following commands after cloning the git repo.
'''
wget https://github.com/AravisProject/aravis/releases/download/0.8.30/aravis-0.8.30.tar.xz -O aravis-0.8.30.tar.xz 
    tar xfJ aravis-0.8.30.tar.xz 
    rm aravis-0.8.30.tar.xz 
    cd aravis-0.8.30 
    meson setup build 
    cd build 
    ninja 
    checkinstall --pkgname aravis --pkgversion 0.8.30 --requires="libglib2.0-dev, libxml2-dev, libusb-1.0-0-dev" ninja install
'''

For scanCONTROL SDK, first compile libmescan
'''
    cd libmescan 
    meson builddir 
    cd builddir
    ninja 
    checkinstall --pkgname mescan --pkgversion ${SCANCONTROL_SDK_VERSION} --requires="aravis \(\>= 0.8.0\)" ninja install
'''
and then libllt
'''
    cd /libllt && \
    meson builddir && \
    cd builddir && \
    ninja && \
    checkinstall --pkgname llt --pkgversion ${SCANCONTROL_SDK_VERSION} --requires="mescan \(\>= ${SCANCONTROL_SDK_VERSION}\),aravis \(\>= 0.8.0\)" ninja install
'''

NOTA : It may be necessary to modifiy llt.h if an error raise during compilation.
Originally lines 27 to 33 are :
'''
#ifdef __cplusplus
extern "C" {
#endif
#include <mescan.h>
#ifdef __cplusplus
}
#endif
'''

replace them by:
'''
#include <arv.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <math.h>
#include <arpa/inet.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <LLTDataTypes.h>
#include <mescan_adv.h>
#include <mescan_basic.h>

#ifdef __cplusplus
}
#endif

'''

#### Building

To build from source, clone the latest version from this repository into your catkin workspace and compile the package using:

	cd colcon_ws/src
	git clone https://gitlab.com/LRJOLY/scancontrol
	cd ../
	colcon build --symlink-install


## Usage

Run the main driver node with:

	ros2 launch micro_epsilon_scancontrol_driver load_driver.launch.py

<!-- ## Config files

* **partial_profile.yaml** Configure custom partial profile settings at start-up. The default values only extract the xyz values from the measurement buffer. Adjust at your own risk! 
	- start_point: 
	- start_point_data: 
	- point_count - Number of data points, defaults to -1 (Inherit from resolution)
	- data_width:  -->

## Launch files

* **driver.launch:** Launch two scanCONTROL driver node and the related configuration window. 
<!-- 
     Arguments

     - **`show_rqt_plugin`** Display the rqt plugin to reconfigure the sensor on start-up. Default: `true`. -->

## Nodes

### scancontrol_driver_node

The scancontrol_driver_node connects to the scanCONTROL device and allows control of most settings through the provided services. By default the driver only extracts the xyz data from the measurement buffer to create the point cloud message. For now the additional measurement data such as reflections are discarted. 

#### Published Topics

* **`/scancontrol_pointcloud`** ([sensor_msgs/PointCloud2])

	The laser scan data filtered by the partial profile settings. The last point(s) may get lost, as a timestamp overwrites the last 4 bytes of the measurement buffer.


#### Services
Most servives are wrappers of the scanCONTROL API. For more information on the available settings and values see the documentation as part of the [scanCONTROL Linux C++ SDK 1.0](https://www.micro-epsilon.com/2D_3D/laser-scanner/Software/downloads/). The rqt plugin uses these services to change the settings during runtime. 

* **`~set_feature`** ([micro_epsilon_scancontrol_msgs/SetFeature])

	Set a feature (setting) on the scanCONTROL device. 


* **`~get_feature`** ([micro_epsilon_scancontrol_msgs/GetFeature])

	Get the current feature (setting) from the connected scanCONTROL device. 

* **`~get_resolution`** ([micro_epsilon_scancontrol_msgs/GetResolution])

	Get the current active resolution used by the connected scanCONTROL device.

* **`~set_resolution`** ([micro_epsilon_scancontrol_msgs/SetResolution])

	Set the resultion of the connected scanCONTROL device.

* **`~get_available_resolutions`** ([micro_epsilon_scancontrol_msgs/GetAvailableResolutions])

	Retrieve a list of all available resolutions of the connected scanCONTROL device. 

* **`~invert_x`** ([std_srvs/SetBool])

	Flip the X values around the middle of the laser line of the sensor.  

* **`~invert_z`** ([std_srvs/SetBool]

	Flip the Z values around the middle of the measuring range of the sensor. Factory default value of of this setting is `True`.

#### Parameters
Device Settings
* **`resolution`** (int)

	Define a prefered resolution setting for the laser scan. By default the highest available resolution is selected.

The following parameters are available to allow using multiple scanCONTROL devices.

* **`serial`** (string)

	Define a prefered scanCONTROL device by its serial number. If not defined, the driver will try to connect to the first device it is able to find. 

* **`topic_name`** (string, default: `scancontrol_pointcloud`)

	Define a custom name for the topic to publish the point cloud data on. 

* **`frame_id`** (string, default: `scancontrol`)

	Define a custom name for the measurement frame in which the point clouds are published.



## Bugs & Feature Requests

Please report bugs and request features using the [Issue Tracker](git clone https://gitlab.com/LRJOLY/scancontrol/issues).


[ROS2]: http://www.ros.org
[scanCONTROL Linux C++ SDK 1.0]: (https://www.micro-epsilon.com/2D_3D/laser-scanner/Software/downloads/)
[sensor_msgs/PointCloud2]: http://docs.ros.org/api/sensor_msgs/html/msg/PointCloud2.html
