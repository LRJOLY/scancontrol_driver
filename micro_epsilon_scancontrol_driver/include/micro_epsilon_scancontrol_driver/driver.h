#ifndef _SCANCONTROL_DRIVER_H_
#define _SCANCONTROL_DRIVER_H_

#include <rclcpp/rclcpp.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

#include <std_srvs/srv/set_bool.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <micro_epsilon_scancontrol_msgs/srv/get_feature.hpp>
#include <micro_epsilon_scancontrol_msgs/srv/set_feature.hpp>
#include <micro_epsilon_scancontrol_msgs/srv/get_resolution.hpp>
#include <micro_epsilon_scancontrol_msgs/srv/set_resolution.hpp>
#include <micro_epsilon_scancontrol_msgs/srv/get_available_resolutions.hpp>

#include <llt.h>
#include <mescan.h>

#define MAX_DEVICE_INTERFACE_COUNT 6
#define MAX_RESOLUTION_COUNT 6
#define GENERAL_FUNCTION_FAILED -1

#define DEFAULT_FRAME_ID "aperture_frame"
#define DEFAULT_TOPIC_NAME "scancontrol_pointcloud"

typedef pcl::PointCloud<pcl::PointXYZI> point_cloud_t;

namespace scancontrol_driver
{
    class ScanControlDriver
    {
        public:
            // Constructor and destructor
            ScanControlDriver(rclcpp::Node::SharedPtr node_handle);
            ~ScanControlDriver() {}

            void init(rclcpp::Node::SharedPtr node_handle);

            // Profile functions
            int SetPartialProfile(int &resolution);
            int StartProfileTransfer();
            int StopProfileTransfer();
            int ProcessAndPublishProfile(const void * data, size_t data_size);
            
            // Device setting functions
            int GetFeature(unsigned int setting_id, unsigned int *value);
            int SetFeature(unsigned int setting_id, unsigned int value);

            // Get configuration parameters 
            std::string serial() const {return config_.serial;};
            int resolution() const {return config_.resolution;};

            // Service Callback
            bool ServiceSetFeature( std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetFeature::Request> request,
                                     std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetFeature::Response> response);
            bool ServiceGetFeature( std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetFeature::Request> request,
                                    std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetFeature::Response> response);
            bool ServiceSetResolution(  std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetResolution::Request> request,
                                        std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetResolution::Response> response);
            bool ServiceGetResolution(  std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetResolution::Request> request,
                                        std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetResolution::Response> response);
            bool ServiceGetAvailableResolutions(    std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetAvailableResolutions::Request> request,
                                                    std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetAvailableResolutions::Response> response);
            bool ServiceInvertZ(    std::shared_ptr<std_srvs::srv::SetBool::Request> request,
                                    std::shared_ptr<std_srvs::srv::SetBool::Response> response);
            bool ServiceInvertX(    std::shared_ptr<std_srvs::srv::SetBool::Request> request,
                                    std::shared_ptr<std_srvs::srv::SetBool::Response> response);
    
        private:
            // Profile functions
            int Profile2PointCloud();
            // Configuration storage
            struct
            {
                std::string frame_id;                     // string
                std::string model;                        // string 
                std::string serial;                       // string
                std::string interface;                    // string
                std::string topic_name;                   // string
                int resolution;                   // int
                int pp_start_point;               // int
                int pp_start_point_data;          // int
                int pp_point_count;               // int
                int pp_point_data_width;          // int
            } config_;
            
            bool initialized_ = false;
            bool transfer_active_ = false;

            // ROS handles
            rclcpp::Node::SharedPtr nh_;
            //ros::NodeHandle private_nh_;
            rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr publisher_ ;

            rclcpp::Service<micro_epsilon_scancontrol_msgs::srv::GetFeature>::SharedPtr get_feature_srv_;
            rclcpp::Service<micro_epsilon_scancontrol_msgs::srv::SetFeature>::SharedPtr set_feature_srv_;
            rclcpp::Service<micro_epsilon_scancontrol_msgs::srv::GetResolution>::SharedPtr get_resolution_srv_;
            rclcpp::Service<micro_epsilon_scancontrol_msgs::srv::SetResolution>::SharedPtr set_resolution_srv_;
            rclcpp::Service<micro_epsilon_scancontrol_msgs::srv::GetAvailableResolutions>::SharedPtr get_available_resolutions_srv_;
            rclcpp::Service<std_srvs::srv::SetBool>::SharedPtr invert_z_srv_;
            rclcpp::Service<std_srvs::srv::SetBool>::SharedPtr invert_x_srv_;


            // Driver objects
            std::unique_ptr<CInterfaceLLT> device_interface_ptr;
            TScannerType device_type;
            TPartialProfile t_partial_profile_;
            std::vector<guint8> profile_buffer;
            std::vector<double> value_x, value_z;
            int lost_values;
            unsigned int lost_profiles;

            std::unique_ptr<point_cloud_t> point_cloud_msg_;

            std::map<std::string, unsigned int> feature2id = {
                {"FEATURE_FUNCTION_SERIAL", FEATURE_FUNCTION_SERIAL},
                {"FEATURE_FUNCTION_PEAKFILTER_WIDTH", FEATURE_FUNCTION_PEAKFILTER_WIDTH},
                {"FEATURE_FUNCTION_PEAKFILTER_HEIGHT", FEATURE_FUNCTION_PEAKFILTER_HEIGHT},
                {"FEATURE_FUNCTION_FREE_MEASURINGFIELD_Z", FEATURE_FUNCTION_FREE_MEASURINGFIELD_Z},
                {"FEATURE_FUNCTION_FREE_MEASURINGFIELD_X", FEATURE_FUNCTION_FREE_MEASURINGFIELD_X},
                {"FEATURE_FUNCTION_DYNAMIC_TRACK_DIVISOR", FEATURE_FUNCTION_DYNAMIC_TRACK_DIVISOR},
                {"FEATURE_FUNCTION_DYNAMIC_TRACK_FACTOR", FEATURE_FUNCTION_DYNAMIC_TRACK_FACTOR},
                {"FEATURE_FUNCTION_CALIBRATION_0", FEATURE_FUNCTION_CALIBRATION_0},
                {"FEATURE_FUNCTION_CALIBRATION_1", FEATURE_FUNCTION_CALIBRATION_1},
                {"FEATURE_FUNCTION_CALIBRATION_2", FEATURE_FUNCTION_CALIBRATION_2},
                {"FEATURE_FUNCTION_CALIBRATION_3", FEATURE_FUNCTION_CALIBRATION_3},
                {"FEATURE_FUNCTION_CALIBRATION_4", FEATURE_FUNCTION_CALIBRATION_4},
                {"FEATURE_FUNCTION_CALIBRATION_5", FEATURE_FUNCTION_CALIBRATION_5},
                {"FEATURE_FUNCTION_CALIBRATION_6", FEATURE_FUNCTION_CALIBRATION_6},
                {"FEATURE_FUNCTION_CALIBRATION_7", FEATURE_FUNCTION_CALIBRATION_7},
                {"FEATURE_FUNCTION_LASERPOWER", FEATURE_FUNCTION_LASERPOWER},
                {"FEATURE_FUNCTION_MEASURINGFIELD", FEATURE_FUNCTION_MEASURINGFIELD},
                {"FEATURE_FUNCTION_TRIGGER", FEATURE_FUNCTION_TRIGGER},
                {"FEATURE_FUNCTION_SHUTTERTIME", FEATURE_FUNCTION_SHUTTERTIME},
                {"FEATURE_FUNCTION_IDLETIME", FEATURE_FUNCTION_IDLETIME},
                {"FEATURE_FUNCTION_PROCESSING_PROFILEDATA", FEATURE_FUNCTION_PROCESSING_PROFILEDATA},
                {"FEATURE_FUNCTION_THRESHOLD", FEATURE_FUNCTION_THRESHOLD},
                {"FEATURE_FUNCTION_MAINTENANCEFUNCTIONS", FEATURE_FUNCTION_MAINTENANCEFUNCTIONS},
                {"FEATURE_FUNCTION_REARRANGEMENT_PROFILE", FEATURE_FUNCTION_REARRANGEMENT_PROFILE},
                {"FEATURE_FUNCTION_PROFILE_FILTER", FEATURE_FUNCTION_PROFILE_FILTER},
                {"FEATURE_FUNCTION_RS422_INTERFACE_FUNCTION", FEATURE_FUNCTION_RS422_INTERFACE_FUNCTION},
                {"FEATURE_FUNCTION_PACKET_DELAY", FEATURE_FUNCTION_PACKET_DELAY},
                {"FEATURE_FUNCTION_SHARPNESS", FEATURE_FUNCTION_SHARPNESS},
                {"FEATURE_FUNCTION_TEMPERATURE", FEATURE_FUNCTION_TEMPERATURE},
                {"FEATURE_FUNCTION_SATURATION", FEATURE_FUNCTION_SATURATION},
                {"FEATURE_FUNCTION_CAPTURE_QUALITY", FEATURE_FUNCTION_CAPTURE_QUALITY}
            };

            void manageParameters();

            std::map<std::string, unsigned int> param2id_ = {
                {"pulse_mode", FEATURE_FUNCTION_LASER},
                {"exposure_time", FEATURE_FUNCTION_EXPOSURE_TIME},
                {"idle_time", FEATURE_FUNCTION_IDLE_TIME},
                {"ea_reference_region", FEATURE_FUNCTION_EA_REFERENCE_REGION_POSITION},
                {"roi1", FEATURE_FUNCTION_ROI1_POSITION},
                {"roi2", FEATURE_FUNCTION_ROI2_POSITION},
                {"threshold", FEATURE_FUNCTION_THRESHOLD},
                {"profile_filter", FEATURE_FUNCTION_PROFILE_FILTER}
            };

            void getParameter(const std::string& feature_name, unsigned int feature_id);         
 
            void setParameter(const std::string& feature_name, unsigned int feature_id);         
 
            std::map<unsigned int, unsigned int> device_params_;



            //
            std::list<unsigned int> features_with_corruption_risk = {FEATURE_FUNCTION_LASERPOWER, FEATURE_FUNCTION_MEASURINGFIELD, FEATURE_FUNCTION_TRIGGER}; 
    };

    void NewProfileCallback(const void * data, size_t data_size, gpointer user_data);
    void ControlLostCallback(ArvGvDevice *mydevice, gpointer user_data);

} // namespace scancontrol_driver

#endif // _SCANCONTROL_DRIVER_H_