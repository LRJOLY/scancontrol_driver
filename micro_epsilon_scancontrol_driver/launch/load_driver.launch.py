from ament_index_python.packages import get_package_share_path

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.actions import DeclareLaunchArgument
from launch.conditions import IfCondition, UnlessCondition
from launch.substitutions import Command, LaunchConfiguration

from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare
from launch_ros.parameter_descriptions import ParameterValue
import os
from ament_index_python import get_package_share_directory

def generate_launch_description():

    model_launch_file = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(get_package_share_directory('micro_epsilon_scancontrol_description'),
                        'launch/load_scancontrol_30xx_430.launch.py')
        )
    )

    driver_node = Node(
        package='micro_epsilon_scancontrol_driver',
        executable='driver_node',
        output='screen',
        parameters=[
            {"serial":"3000-430-523100006"},
            {"topic_name":"scancontrol100_pointcloud"}
        ] )

    second_model_launch_file = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(get_package_share_directory('micro_epsilon_scancontrol_description'),
                        'launch/load_scancontrol_30xx_430.launch.py')
        )
    )

    second_driver_node = Node(
        package='micro_epsilon_scancontrol_driver',
        executable='driver_node',
        output='screen',
        parameters=[
            {"serial":"3000-430-523100005"},
            {"topic_name":"scancontrol430_pointcloud"}
        ] )

    return LaunchDescription([
        #driver_node,
        #model_launch_file,
        second_driver_node,
        second_model_launch_file,
    ])
