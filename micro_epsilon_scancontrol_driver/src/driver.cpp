#include "micro_epsilon_scancontrol_driver/driver.h"

using namespace std::chrono;

namespace scancontrol_driver
{
    ScanControlDriver::ScanControlDriver(rclcpp::Node::SharedPtr node_handle) : nh_(node_handle)
    {   
         
        //    Store the ros::NodeHandle objects and extract the relevant parameters. 
           
        nh_         = node_handle;


        RCLCPP_WARN_STREAM(nh_->get_logger(), "param begin");

        // Declare params
        nh_->declare_parameter("resolution", -1);
        nh_->declare_parameter("serial", std::string(""));
        nh_->declare_parameter("frame_id", std::string(DEFAULT_FRAME_ID));
        nh_->declare_parameter("topic_name", std::string(DEFAULT_TOPIC_NAME));
        nh_->declare_parameter("partial_profile_start_point", 0);
        nh_->declare_parameter("partial_profile_start_point_data", 4);
        nh_->declare_parameter("partial_profile_point_count", -1);
        nh_->declare_parameter("partial_profile_data_width", 4);

        // Device settings
        config_.resolution = nh_->get_parameter("resolution").as_int();
        // Multiple device parameters
        config_.serial = nh_->get_parameter("serial").as_string();
        config_.frame_id = nh_->get_parameter("frame_id").as_string();
        config_.topic_name = nh_->get_parameter("topic_name").as_string();

        // TODO: Are these parameters needed?
        config_.pp_start_point = nh_->get_parameter("partial_profile_start_point").as_int();
        config_.pp_start_point_data = nh_->get_parameter("partial_profile_start_point_data").as_int();
        config_.pp_point_count = nh_->get_parameter("partial_profile_point_count").as_int();
        config_.pp_point_data_width = nh_->get_parameter("partial_profile_data_width").as_int();

        // Create driver interface object:
        device_interface_ptr = std::make_unique<CInterfaceLLT>();
        
        /*    Search for available scanCONTROL interfaces
                The code only supports a maximum of MAX_LLT_INTERFACE_COUNT (6) devices. If
                more are connected the devices found after the 6th are not considdered. This
                is a limitation of the API, which needs a fixed number of devices to search 
                for. If 6 devices is insufficient, redefine MAX_LLT_INTERFACE_COUNT to 
                accomodate the additional devices.
        */
        
        gint32 return_code      = 0;
        gint32 interface_count  = 0;
        std::vector<char *> available_interfaces(MAX_DEVICE_INTERFACE_COUNT);

        return_code = device_interface_ptr->GetDeviceInterfaces(&available_interfaces[0], MAX_DEVICE_INTERFACE_COUNT);
        if (return_code == ERROR_GETDEVINTERFACE_REQUEST_COUNT){
            RCLCPP_WARN_STREAM(nh_->get_logger(), "There are more than " << MAX_DEVICE_INTERFACE_COUNT << " scanCONTROL sensors connected.");
            interface_count = MAX_DEVICE_INTERFACE_COUNT;
        } else if (return_code < 0) {
            RCLCPP_WARN_STREAM(nh_->get_logger(), "An error occured while searching for connected scanCONTROL devices. Code: " << return_code);
            interface_count = 0;
        } else {
            interface_count = return_code;
        }
        
        /*    Select scanCONTROL interface
                A preffered interface can be set by means of the 'serial' parameter. 
        */

        gint8 selected_interface = -1;
        if (interface_count == 0){
            RCLCPP_WARN(nh_->get_logger(),"There is no scanCONTROL device connected. Exiting...");
            goto stop_initialization;
        } else if (interface_count == 1){
            RCLCPP_INFO(nh_->get_logger(),"There is 1 scanCONTROL device connected.");
            selected_interface = 0;

            // Check if the available device is the same as the prefered device (if a serial is provided):
            std::string interface(available_interfaces[0]);
            if ((config_.serial == "") || (interface.find(config_.serial) != std::string::npos)){
                RCLCPP_INFO_STREAM(nh_->get_logger(),"Interface found: " << interface);
            }
            else{
                RCLCPP_WARN_STREAM(nh_->get_logger(),"Interface not found! Searched for serial = " << config_.serial);
                RCLCPP_INFO_STREAM(nh_->get_logger(),"Selected interface: " << interface);
            }
        } else {
            RCLCPP_INFO_STREAM(nh_->get_logger(),"There are " << interface_count << " scanCONTROL devices connected.");

            // Select prefered device based on the defined ip or serial. If both are set, this selects the device which ip or serial is encountered first.
            if (config_.serial != ""){
                for (int i = 0; i < interface_count; i++){
                    std::string interface(available_interfaces[i]);
                    if (interface.find(config_.serial) != std::string::npos){
                        RCLCPP_INFO_STREAM(nh_->get_logger(),"Interface found: " << interface);
                        selected_interface = i;
                        break;
                    }
                }
                // Fallback if serial are not found:
                if (selected_interface == -1){
                    RCLCPP_WARN_STREAM(nh_->get_logger(),"Interface not found! Searched for serial = " << config_.serial);
                    RCLCPP_WARN(nh_->get_logger(),"Available interfaces:");
                    for (gint8 i = 0; i < interface_count; i++){
                        RCLCPP_WARN_STREAM(nh_->get_logger(),"   " << available_interfaces[i]);
                    }
                    selected_interface = 0;
                    RCLCPP_INFO_STREAM(nh_->get_logger(),"\nSelecting first available interface: " << available_interfaces[selected_interface]);
                }
            } else{
                selected_interface = 0;
                RCLCPP_INFO_STREAM(nh_->get_logger(),"No 'serial' set, selecting first interface: " << available_interfaces[selected_interface]);
            }
        }

        
        //    Set the selected device to the driver interface class and catch possible errors
        
        config_.interface = std::string(available_interfaces[selected_interface]);
        config_.serial = std::string(config_.interface.end() - 9, config_.interface.end());
        return_code = device_interface_ptr->SetDeviceInterface(available_interfaces[selected_interface]);
        if (return_code < GENERAL_FUNCTION_OK){
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"Error while setting device ID! Code: " << return_code);
            goto stop_initialization;
        }

        
        //    Connect to scanCONTROL device
        
        return_code = device_interface_ptr->Connect();  
        if (return_code < GENERAL_FUNCTION_OK){
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"Error while connecting to device! Code: " << return_code);
            goto stop_initialization;
        }

        
        //    Identify device type and store information on type and serial
        
        return_code = device_interface_ptr->GetLLTType(&device_type);
        if (return_code < GENERAL_FUNCTION_OK){
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"Error while retrieving device type! Code: " << return_code);
            goto stop_initialization;
        }
        if (device_type >= scanCONTROL27xx_25 && device_type <= scanCONTROL27xx_xxx) {
            RCLCPP_INFO_STREAM(nh_->get_logger(),"The scanCONTROL is a scanCONTROL27xx, with serial number " << config_.serial << ".");
            config_.model = std::string("scanCONTROL27xx");
        } else if (device_type >= scanCONTROL26xx_25 && device_type <= scanCONTROL26xx_xxx) {
            RCLCPP_INFO_STREAM(nh_->get_logger(),"The scanCONTROL is a scanCONTROL26xx, with serial number " << config_.serial << ".");
            config_.model = std::string("scanCONTROL26xx");
        } else if (device_type >= scanCONTROL29xx_25 && device_type <= scanCONTROL29xx_xxx) {
            RCLCPP_INFO_STREAM(nh_->get_logger(),"The scanCONTROL is a scanCONTROL29xx, with serial number " << config_.serial << ".");
            config_.model = std::string("scanCONTROL29xx");
        } else if (device_type >= scanCONTROL30xx_25 && device_type <= scanCONTROL30xx_xxx) {
            RCLCPP_INFO_STREAM(nh_->get_logger(),"The scanCONTROL is a scanCONTROL30xx, with serial number " << config_.serial << ".");
            config_.model = std::string("scanCONTROL30xx");
        } else if (device_type >= scanCONTROL25xx_25 && device_type <= scanCONTROL25xx_xxx) {
            RCLCPP_INFO_STREAM(nh_->get_logger(),"The scanCONTROL is a scanCONTROL25xx, with serial number " << config_.serial << ".");
            config_.model = std::string("scanCONTROL25xx");
        } else {
            RCLCPP_FATAL(nh_->get_logger(),"The scanCONTROL device is a undefined type.\nPlease contact Micro-Epsilon for a newer SDK or update the driver.");
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"scanCONTROL device code is " << device_type << ".");
            goto stop_initialization;
        }

        
        //    Set the resolution of the scanner.
        
        // Find all available resolutions
        guint32 available_resolutions[MAX_RESOLUTION_COUNT];
        if ( (return_code = device_interface_ptr->GetResolutions(&available_resolutions[0], MAX_RESOLUTION_COUNT)) < GENERAL_FUNCTION_OK){
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"Unable to request the available resolutions of the scanCONTROL device. Code: " << return_code);
            goto stop_initialization;
        }

        // Select prefered/first found resolution
        if (config_.resolution > 0){
            gint8 selected_resolution = -1;
            for (int i = 0; i < return_code; i++){
                std::string resolution = std::to_string(available_resolutions[i]);
                if (resolution.find(config_.serial) != std::string::npos){  // LRJ : replace "> -1" by "!= std::string::npos"
                    selected_resolution = i;
                    break;
                }
            }
            if (selected_resolution == -1){
                RCLCPP_WARN_STREAM(nh_->get_logger(),"Requested resolution of " << std::to_string(config_.resolution) <<" not found as available option.");
                RCLCPP_WARN(nh_->get_logger(),"Available resolutions:");
                for (int i = 0; i < return_code; i++){
                    RCLCPP_WARN_STREAM(nh_->get_logger(),"   " << std::to_string(available_resolutions[i]));
                }
                config_.resolution = available_resolutions[0];
                RCLCPP_INFO_STREAM(nh_->get_logger(),"Selecting first available resolution: " << std::to_string(config_.resolution));
            }
        } else{
            config_.resolution = available_resolutions[0];
            RCLCPP_INFO_STREAM(nh_->get_logger(),"No resolution set, selecting first available: " << std::to_string(config_.resolution));
        }



        // Set the selected resolution
        if ( (return_code = device_interface_ptr->SetResolution(config_.resolution)) < GENERAL_FUNCTION_OK){
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"Error while setting device resolution! CodeL " << return_code);
            goto stop_initialization;
        }
        
        
        //    Prepare the partial profile
        
        if ((return_code = SetPartialProfile(config_.resolution)) < GENERAL_FUNCTION_OK){
            goto stop_initialization;
        }

        
        /*    Register callback functions
                RegisterBufferCallback > NewProfile: Triggered when the sensor has a new profile available in the buffer.  
                RegisterControlLostCallback > ControlLostCallback: Triggered when control of the device is lost.
        */
        
        if ((return_code = device_interface_ptr->RegisterBufferCallback((gpointer) &NewProfileCallback, this)) < GENERAL_FUNCTION_OK){
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"Error while registering buffer callback. Code: " << return_code);
            goto stop_initialization;
        }
        if ((return_code = device_interface_ptr->RegisterControlLostCallback((gpointer) &ControlLostCallback, this)) < GENERAL_FUNCTION_OK){
            RCLCPP_FATAL_STREAM(nh_->get_logger(),"Error while registering control lost callback. Code: " << return_code);
            goto stop_initialization;
        }

        //    Initialization finished and successfully connected to scanCONTROL device.
        
        initialized_ = true;

        // goto location when initialization fails
        stop_initialization:            
            if (!initialized_) {
                throw std::runtime_error("ScanControlDriver: Initialization failed.");
            }

        // Advertise topic
        publisher_           = nh_->create_publisher<sensor_msgs::msg::PointCloud2>(config_.topic_name, 10);
        
        // Advertise services
        auto get_feat_cb = std::bind(&ScanControlDriver::ServiceGetFeature, this, std::placeholders::_1, std::placeholders::_2);
        get_feature_srv_ = nh_->create_service<micro_epsilon_scancontrol_msgs::srv::GetFeature>(    "get_feature",
                                                                                                    get_feat_cb);
        auto set_feat_cb = std::bind(&ScanControlDriver::ServiceSetFeature, this, std::placeholders::_1, std::placeholders::_2);
        set_feature_srv_ = nh_->create_service<micro_epsilon_scancontrol_msgs::srv::SetFeature>(    "set_feature",
                                                                                                    set_feat_cb);
        auto get_resol_cb = std::bind(&ScanControlDriver::ServiceGetResolution, this, std::placeholders::_1, std::placeholders::_2);
        get_resolution_srv_ = nh_->create_service<micro_epsilon_scancontrol_msgs::srv::GetResolution> ( "get_resolution",
                                                                                                        get_resol_cb);

        auto set_resol_cb = std::bind(&ScanControlDriver::ServiceSetResolution, this, std::placeholders::_1, std::placeholders::_2);
        set_resolution_srv_ = nh_->create_service<micro_epsilon_scancontrol_msgs::srv::SetResolution>(  "set_resolution",
                                                                                                        set_resol_cb); 

        auto get_avail_resol_cb = std::bind(&ScanControlDriver::ServiceGetAvailableResolutions, this, std::placeholders::_1, std::placeholders::_2);
        get_available_resolutions_srv_ = nh_->create_service<micro_epsilon_scancontrol_msgs::srv::GetAvailableResolutions>( "get_available_resolutions",
                                                                                                                            get_avail_resol_cb);

        auto invert_z_cb = std::bind(&ScanControlDriver::ServiceInvertZ, this, std::placeholders::_1, std::placeholders::_2);
        invert_z_srv_ = nh_->create_service<std_srvs::srv::SetBool>(    "invert_z",
                                                                        invert_z_cb);

        auto invert_x_cb = std::bind(&ScanControlDriver::ServiceInvertX, this, std::placeholders::_1, std::placeholders::_2);
        invert_x_srv_  = nh_->create_service<std_srvs::srv::SetBool>(   "invert_x",
                                                                        invert_x_cb);

    }

    void ScanControlDriver::manageParameters() {
        for (auto& param : param2id_) {
            getParameter(param.first, param.second);
        }

        for (const auto& param : param2id_) {
            setParameter(param.first, param.second);
        }
    }

    void scancontrol_driver::ScanControlDriver::getParameter(const std::string& feature_name, unsigned int feature_id) {

        try 
        {
            nh_->declare_parameter(feature_name, rclcpp::PARAMETER_INTEGER);
            unsigned int output_value_tmp = nh_->get_parameter(feature_name).as_int();
            device_params_.insert({feature_id,output_value_tmp});
            RCLCPP_INFO(nh_->get_logger(), " %s is set to : %u", feature_name.c_str(), output_value_tmp);
        } 
        catch (const std::exception& e) {
        /   / In case of absence or type error, retrieve value currently in sensor
            RCLCPP_ERROR(nh_->get_logger(), "%s not found in YAML file or type mismatch in config: %s", feature_name.c_str(), e.what());
            RCLCPP_ERROR(nh_->get_logger(), "actual configuration of the sensor will be used");
            unsigned int value;
            int return_code = device_interface_ptr->GetFeature(feature_id, value);
            if (return_code < 0) {
                RCLCPP_ERROR(nh_->get_logger(), "Error while getting parameter %s actually in use", feature_name.c_str());
            } else {
                device_params_.insert({feature_id,output_value_tmp});
                RCLCPP_INFO(nh_->get_logger(), "%s will stay equal to: %u", feature_name.c_str(), value);
            }
        }
    }

    void scancontrol_driver::ScanControlDriver::setParameter(const std::string& feature_name, unsigned int feature_id) {
        int return_code = device_interface_ptr->SetFeature(feature_id, device_params_.at(feature_id));
        if (return_code < 0) {
            RCLCPP_ERROR(nh_->get_logger(), "Error while setting %s", feature_name.c_str());
        } else {
            RCLCPP_INFO(nh_->get_logger(), "%s is set to: %u", feature_name.c_str(), device_params_.at(feature_id));
        }
    }

    int ScanControlDriver::SetPartialProfile(int &resolution){
        gint32 return_code = 0;
        // Set profile configuration to partial profile
        if ((return_code = device_interface_ptr->SetProfileConfig(PARTIAL_PROFILE)) < GENERAL_FUNCTION_OK){
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Error while setting profile config to PARTIAL_PROFILE. Code: " << return_code);
            return GENERAL_FUNCTION_FAILED;
        }

        // Create partial profile object and send to device
        t_partial_profile_.nStartPoint = config_.pp_start_point;
        t_partial_profile_.nStartPointData = config_.pp_start_point_data;
        t_partial_profile_.nPointDataWidth = config_.pp_point_data_width;
        t_partial_profile_.nPointCount = (config_.pp_point_count == -1 || config_.pp_point_count > resolution) ? resolution : config_.pp_point_count;

        // Send partial profile settings to device
        if ((return_code = device_interface_ptr->SetPartialProfile(&t_partial_profile_)) < GENERAL_FUNCTION_OK){
            // Restore nPointCount to old value 
            t_partial_profile_.nPointCount = (config_.pp_point_count == -1 || config_.pp_point_count > resolution) ? config_.resolution : config_.pp_point_count;
            
            // Send warning and return failed
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Error while setting partial profile settings. Code: " << return_code);
            return GENERAL_FUNCTION_FAILED;
        }

        // Resize buffers - values contain less than the point count, due to the timestamp data
        profile_buffer.resize(t_partial_profile_.nPointCount*t_partial_profile_.nPointDataWidth);
        lost_values = (16 + t_partial_profile_.nPointDataWidth - 1)/t_partial_profile_.nPointDataWidth;
        value_x.resize(t_partial_profile_.nPointCount);
        value_z.resize(t_partial_profile_.nPointCount);
        RCLCPP_INFO_STREAM(nh_->get_logger(),"Profile is losing " << std::to_string(lost_values) << " values due to timestamp of 16 byte at the end of the profile.");

        // Prepare new point cloud message
        point_cloud_msg_.reset(new point_cloud_t);
        point_cloud_msg_->header.frame_id = config_.frame_id;
        point_cloud_msg_->height = 1;
        point_cloud_msg_->width = config_.resolution;
        for (int i=0; i<config_.resolution; i++){
            pcl::PointXYZI point(1.0);
            point_cloud_msg_->points.push_back(point);
        }

        return GENERAL_FUNCTION_OK;
    }


    // Start the transfer of new profiles 
    int ScanControlDriver::StartProfileTransfer(){
        int return_code = 0;
        if ((return_code = device_interface_ptr->TransferProfiles(NORMAL_TRANSFER, true)) < GENERAL_FUNCTION_OK) {
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Error while starting profile transfer! Code: " << return_code);
            return return_code;
        }
        transfer_active_ = true;
        return GENERAL_FUNCTION_OK;
    }


    // Stop the transfer of new profiles 
    int ScanControlDriver::StopProfileTransfer(){
        int return_code = 0;
        if ((return_code = device_interface_ptr->TransferProfiles(NORMAL_TRANSFER, false)) < GENERAL_FUNCTION_OK) {
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Error while stopping profile transfer. Code: " << return_code);
            return return_code;
        }
        transfer_active_ = false;
        return GENERAL_FUNCTION_OK;
    }

    // Process raw profile data and create the point cloud message 
    int ScanControlDriver::Profile2PointCloud(){

        device_interface_ptr->ConvertPartProfile2Values(&profile_buffer[0], profile_buffer.size(), &t_partial_profile_, device_type, 0, NULL, NULL, NULL, &value_x[0], &value_z[0], NULL, NULL);
        for (int i = 0; i < config_.resolution; i++){
            point_cloud_msg_->points[i].x = value_x[i]/1000;
            
            // Fill in NaN if the scanner is to close or far away (sensor returns ~32.232) and for the final few points which are overwritten by the timestamp data
            if ((value_z[i] < 32.5) || (i >= config_.resolution - lost_values)){
                point_cloud_msg_->points[i].z = std::numeric_limits<double>::infinity(); 
            }
            else{
                point_cloud_msg_->points[i].z = value_z[i]/1000;
            }
        }
        return GENERAL_FUNCTION_OK;
    }
    
    // Process and publish profile 
    int ScanControlDriver::ProcessAndPublishProfile(const void * data, size_t data_size){
        // Timestamp 
        pcl_conversions::toPCL(nh_->get_clock()->now(), point_cloud_msg_->header.stamp);

        // Copy sensor data to local buffer 
        if (data != NULL && data_size == profile_buffer.size()){
            memcpy(&profile_buffer[0], data, data_size);
        }

        // Process buffer and publish point cloud
        ScanControlDriver::Profile2PointCloud();
        sensor_msgs::msg::PointCloud2 ros_msg;
        pcl::toROSMsg(*point_cloud_msg_, ros_msg);
        publisher_->publish(ros_msg);

        return GENERAL_FUNCTION_OK;
    }

    // Retrieve the current value of a setting/feature. Consult the scanCONTROL API documentation for a list of available features 
    int ScanControlDriver::GetFeature(unsigned int setting_id, unsigned int *value){
        int return_code = 0;
        if ((return_code = device_interface_ptr->GetFeature(setting_id, value)) < GENERAL_FUNCTION_OK){
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Setting could not be retrieved. Code: " << return_code);
            return return_code;
        }
        return GENERAL_FUNCTION_OK;
    }

    
    //    Attempt to set the a setting/feature to the requested value. 
    //        The function temporarily halts the transfer of new profiles if the setting can cause the profiles to be corrupted. 
     
    int ScanControlDriver::SetFeature(unsigned int setting_id, unsigned int value){
        int return_code = 0;
        if ((std::find(features_with_corruption_risk.begin(), features_with_corruption_risk.end(), setting_id) != features_with_corruption_risk.end()) && transfer_active_){
            RCLCPP_INFO(nh_->get_logger(),"Risk of profile corruption, temporarily stopping profile transfer.");
            if ( (return_code = ScanControlDriver::StopProfileTransfer()) < GENERAL_FUNCTION_OK){
                RCLCPP_WARN_STREAM(nh_->get_logger(),"Profile transfer could not be stopped. Code: " << return_code);
                return -1;
            }
            if ( (return_code = device_interface_ptr->SetFeature(setting_id, value)) < GENERAL_FUNCTION_OK){
                RCLCPP_WARN_STREAM(nh_->get_logger(),"Feature could not be set. Code: " << return_code);
                return return_code;
            }
            if ( (return_code = ScanControlDriver::StartProfileTransfer()) < GENERAL_FUNCTION_OK){
                RCLCPP_WARN_STREAM(nh_->get_logger(),"Profile transfer could not be restarted after changing feature. Code: " << return_code);
                return -1;
            }
            return GENERAL_FUNCTION_OK;
        }
        if ( (return_code = device_interface_ptr->SetFeature(setting_id, value)) < GENERAL_FUNCTION_OK){
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Feature could not be set. Code: " << return_code);
            return return_code;
        }
        return GENERAL_FUNCTION_OK;
    }

    // Wrapper of the SetFeature call for use by the ServiceSetFeature service 
    bool ScanControlDriver::ServiceSetFeature(  std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetFeature::Request> request,
                                                std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetFeature::Response> response){
        response->return_code = ScanControlDriver::SetFeature(request->setting, request->value);
        return true;
    }
    // Wrapper of the GetFeature call for use by the ServiceGetFeature service 
      bool ScanControlDriver::ServiceGetFeature(    std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetFeature::Request> request,
                                                    std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetFeature::Response> response){
        response->return_code = ScanControlDriver::GetFeature(request->setting, &(response->value));
        return true;
    }
    // Wrapper of the SetResolution call for use by the ServiceSetResolution service 
    bool ScanControlDriver::ServiceSetResolution(   std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetResolution::Request> request,
                                                    std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::SetResolution::Response> response){
        if ( (response->return_code = StopProfileTransfer()) < GENERAL_FUNCTION_OK){
            return true;
        }
        if ( (response->return_code = device_interface_ptr->SetResolution(request->resolution)) < GENERAL_FUNCTION_OK){
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Error while setting device resolution! Code: " << response->return_code);
            return true;
        }
        int temp_resolution = request->resolution;
        if ( (response->return_code = SetPartialProfile(temp_resolution)) < GENERAL_FUNCTION_OK){
            RCLCPP_WARN_STREAM(nh_->get_logger(),"Error while setting partial profile. Code: " << response->return_code);
            return true;
        }
        if ( (response->return_code = StartProfileTransfer()) < GENERAL_FUNCTION_OK){
            return true;
        }

        // Change of resolution was succesull
        config_.resolution = request->resolution;
        return true;
    }

    // Wrapper of the GetResolution call for use by the ServiceGetResolution service
    bool ScanControlDriver::ServiceGetResolution(   std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetResolution::Request> request,
                                                    std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetResolution::Response> response){
        response->return_code = device_interface_ptr->GetResolution(&(response->resolution));
        return true;
    }
    
    // Wrapper of the GetResolutions call for use by the ServiceGetAvailableResolutions service 
    bool ScanControlDriver::ServiceGetAvailableResolutions( std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetAvailableResolutions::Request> request,
                                                            std::shared_ptr<micro_epsilon_scancontrol_msgs::srv::GetAvailableResolutions::Response> response){
        guint32 available_resolutions[MAX_RESOLUTION_COUNT] = {0};
        response->return_code = device_interface_ptr->GetResolutions(&available_resolutions[0], MAX_RESOLUTION_COUNT);
        for (int i = 0; i < MAX_RESOLUTION_COUNT; i++){
            if (available_resolutions[i] > 0){
                response->resolutions.push_back(available_resolutions[i]);
            }  
        }
        return true;
    }
    /* 
    Enable or disable the inversion of Z values on the scanCONTROL device:
        If request.data == true > Enable the inversion of Z values on the scanCONTROL device. (Default of the scanCONTROL device)
        If request.data == false > Disable the inversion of Z values on the scanCONTROL device.
    */
    bool ScanControlDriver::ServiceInvertZ( std::shared_ptr<std_srvs::srv::SetBool::Request> request,
                                            std::shared_ptr<std_srvs::srv::SetBool::Response> response)
    {   
        unsigned int value;
        int return_code; 

        // Retrieve current settings
        if ( (return_code = ScanControlDriver::GetFeature(FEATURE_FUNCTION_PROCESSING_PROFILEDATA ,&value)) < GENERAL_FUNCTION_OK)
        {   
            // Failed to get PROCESSING feature
            response->success = false;
            response->message = std::string("Failed to get 'Profile Data Processing' feature. Error code: ") + std::to_string(return_code);
            return true;
        }

        // Set 6th bit according to the SetBool service request
        value = value & ~(1<<6);
        if (request->data) 
        {
            value |= (1<<6);
        }

        // Send the updated settings 
        if ( (return_code = ScanControlDriver::SetFeature(FEATURE_FUNCTION_PROCESSING_PROFILEDATA, value)) < GENERAL_FUNCTION_OK)
        {
            // Failed to set PROCESSING feature
            response->success = false;
            response->message = std::string("Failed to set 'Profile Data Processing' feature. Error code: ") + std::to_string(return_code);
            return true;
        }

        response->success = true;

        return true;
    }  

    /* 
    Enable or disable the inversion of X values on the scanCONTROL device:
        If request.data == true > Enable the inversion of X values on the scanCONTROL device. (Default of the scanCONTROL device)
        If request.data == false > Disable the inversion of X values on the scanCONTROL device.
    */
    bool ScanControlDriver::ServiceInvertX( std::shared_ptr<std_srvs::srv::SetBool::Request> request,
                                            std::shared_ptr<std_srvs::srv::SetBool::Response> response)
    {   
        unsigned int value;
        int return_code; 

        // Retrieve current settings
        if ( (return_code = ScanControlDriver::GetFeature(FEATURE_FUNCTION_PROCESSING_PROFILEDATA ,&value)) < GENERAL_FUNCTION_OK)
        {   
            // Failed to get PROCESSING feature
            response->success = false;
            response->message = std::string("Failed to get 'Profile Data Processing' feature. Error code: ") + std::to_string(return_code);
            return true;
        }

        // Set 6th bit according to the SetBool service request
        value = value & ~(1<<7);
        if (request->data) 
        {
            value |= (1<<7);
        }

        // Send the updated settings 
        if ( (return_code = ScanControlDriver::SetFeature(FEATURE_FUNCTION_PROCESSING_PROFILEDATA, value)) < GENERAL_FUNCTION_OK)
        {
            // Failed to set PROCESSING feature
            response->success = false;
            response->message = std::string("Failed to set 'Profile Data Processing' feature. Error code: ") + std::to_string(return_code);
            return true;
        }

        response->success = true;

        return true;
    }  


    // Callback for when a new profile is read, for use with the scanCONTROL API. 
    void NewProfileCallback(const void * data, size_t data_size, gpointer user_data){
        // Cast user_data to a driver class pointer and process and publish point cloud
        ScanControlDriver* driver_ptr = static_cast<ScanControlDriver*>(user_data);

        // Call member function
        driver_ptr->ProcessAndPublishProfile(data, data_size);
    }

    // Callback for when connection to the device is lost, for use with the scanCONTROL API. 
    void ControlLostCallback(ArvGvDevice *mydevice, gpointer user_data){
        //TODO : RCLCPP_FATAL(nh_->get_logger(),"Conrol of scanCONTROL device lost!");
        rclcpp::shutdown();
    }


} // namespace scancontrol_driver