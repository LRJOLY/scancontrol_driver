
#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include <rclcpp/rclcpp.hpp>
#include "micro_epsilon_scancontrol_driver/driver.h"

using namespace std::chrono_literals;


class DriverNode : public rclcpp::Node
{
    public:
        DriverNode()
        : Node("micro_epsilon_scancontrol_driver_node"){
        }

        void initialisation() {
            driver_ = std::make_shared<scancontrol_driver::ScanControlDriver>(shared_from_this());
        }

        void start_profile_transfer()
        {
            driver_->StartProfileTransfer();
        }

        void stop_profile_transfer()
        {
            driver_->StopProfileTransfer();
        }

    private:
        std::shared_ptr<scancontrol_driver::ScanControlDriver> driver_;
};



int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    rclcpp::executors::SingleThreadedExecutor executor;
    auto node_handle = std::make_shared<DriverNode>();
    executor.add_node(node_handle);

    using namespace std::literals::chrono_literals;
    auto timeout = 30ns;
    // Start the driver
    try
    {
        node_handle->initialisation();
        RCLCPP_INFO(node_handle->get_logger(), "Driver started");

        // Loop driver until shutdown
        node_handle->start_profile_transfer();
        while (rclcpp::ok())
        {
            executor.spin_once(timeout);
        }
        node_handle->stop_profile_transfer();
        return 0;
    }
    catch (const std::runtime_error& error)
    {
        RCLCPP_FATAL_STREAM(node_handle->get_logger(), error.what());
        rclcpp::shutdown();
        return 0;
    }
}